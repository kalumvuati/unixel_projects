<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{

    /**
    * @Route("/", name="home")
    * @return Response
    */
    public function home()
    {
        $active = "home";
        return $this->render('home/home.html.twig', ['active'=>$active]);
    }

    /**
    * @Route("/gallery", name="gallery")
    * @return Response
    */
    public function gallery()
    {
        $active = "gallery";
        return $this->render('home/gallery.html.twig', ['active'=>$active]);
    }

    /**
    * @Route("/news", name="news")
    * @return Response
    */
    public function news()
    {
        $active = "news";
        return $this->render('home/news.html.twig', ['active'=>$active]);
    }

    /**
    * @Route("/price", name="price")
    * @return Response
    */
    public function price()
    {
        $active = "price";
        return $this->render('home/price.html.twig', ['active'=>$active]);
    }

    /**
    * @Route("/contact", name="contact")
    * @return Response
    */
    public function contact()
    {
        $active = "contact";
        return $this->render('home/contact.html.twig', ['active'=>$active]);
    }

    /**
    * @Route("/ugc", name="ugc")
    * @return Response
    */
    public function ugc()
    {
        $active = "ugc";
        return $this->render('home/ugc.html.twig', ['active'=>$active]);
    }

    /**
    * @Route("/legales", name="legales")
    * @return Response
    */
    public function legales()
    {
        $active = "legales";
        return $this->render('home/legales.html.twig', ['active'=>$active]);
    }
}
