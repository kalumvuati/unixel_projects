<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/portfolio")
 * Class PortfolioController
 * @package App\Controller
 */
class PortfolioController extends AbstractController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @Route("/", name="portfolio")
     * @return Response
     */
    public function index(): Response
    {
        $active = "portfolio";
        $title = $this->translator->trans($active . 's');
        return $this->render('home/portfolio.html.twig', ['active'=>$active, 'title'=>$title]);
    }

    /**
     * @Route("/{slug}", name="portfolio_show")
     * @param string $slug
     * @return Response
     */
    public function show(string $slug): Response
    {
        $active = "portfolio";
        return $this->render('home/portfolio_ref.html.twig', ['active'=>$active]);
    }
}
