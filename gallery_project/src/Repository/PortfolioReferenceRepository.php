<?php

namespace App\Repository;

use App\Entity\PortfolioReference;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PortfolioReference|null find($id, $lockMode = null, $lockVersion = null)
 * @method PortfolioReference|null findOneBy(array $criteria, array $orderBy = null)
 * @method PortfolioReference[]    findAll()
 * @method PortfolioReference[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PortfolioReferenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PortfolioReference::class);
    }

    // /**
    //  * @return PortfolioReference[] Returns an array of PortfolioReference objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PortfolioReference
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
