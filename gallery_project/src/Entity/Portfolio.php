<?php

namespace App\Entity;

use App\Repository\PortfolioRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PortfolioRepository::class)
 */
class Portfolio
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $title;

    /**
     * @ORM\Column(type="integer")
     */
    private $nb_like;

    /**
     * @ORM\Column(type="integer")
     */
    private $nb_unliked;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imagefile;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="portfolios")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=PortfolioReference::class, mappedBy="portfolio", orphanRemoval=true)
     */
    private $refferences;

    public function __construct()
    {
        $this->refferences = new ArrayCollection();
        $this->created_at = new DateTime("now");
        $this->updated_at = new DateTime("now");
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getNbLike(): ?int
    {
        return $this->nb_like;
    }

    public function setNbLike(int $nb_like): self
    {
        $this->nb_like = $nb_like;

        return $this;
    }

    public function getNbUnliked(): ?int
    {
        return $this->nb_unliked;
    }

    public function setNbUnliked(int $nb_unliked): self
    {
        $this->nb_unliked = $nb_unliked;

        return $this;
    }

    public function getImagefile(): ?string
    {
        return $this->imagefile;
    }

    public function setImagefile(string $imagefile): self
    {
        $this->imagefile = $imagefile;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|PortfolioReference[]
     */
    public function getRefferences(): Collection
    {
        return $this->refferences;
    }

    public function addRefference(PortfolioReference $refference): self
    {
        if (!$this->refferences->contains($refference)) {
            $this->refferences[] = $refference;
            $refference->setPortfolio($this);
        }

        return $this;
    }

    public function removeRefference(PortfolioReference $refference): self
    {
        if ($this->refferences->removeElement($refference)) {
            // set the owning side to null (unless already changed)
            if ($refference->getPortfolio() === $this) {
                $refference->setPortfolio(null);
            }
        }

        return $this;
    }
}
