<?php

namespace App\Entity;

use App\Repository\VisitorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VisitorRepository::class)
 */
class Visitor
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $remote_addr;

    /**
     * @ORM\Column(type="integer")
     */
    private $nb_times_visited;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $last_visite_at;

    /**
     * @ORM\ManyToMany(targetEntity=News::class, mappedBy="visitors")
     */
    private $news;

    /**
     * @ORM\ManyToMany(targetEntity=News::class, mappedBy="likes")
     */
    private $news_liked;

    /**
     * @ORM\OneToMany(targetEntity=Message::class, mappedBy="visitor")
     */
    private $messages;

    public function __construct()
    {
        $this->news = new ArrayCollection();
        $this->news_liked = new ArrayCollection();
        $this->messages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRemoteAddr(): ?string
    {
        return $this->remote_addr;
    }

    public function setRemoteAddr(string $remote_addr): self
    {
        $this->remote_addr = $remote_addr;

        return $this;
    }

    public function getNbTimesVisited(): ?int
    {
        return $this->nb_times_visited;
    }

    public function setNbTimesVisited(int $nb_times_visited): self
    {
        $this->nb_times_visited = $nb_times_visited;

        return $this;
    }

    public function getLastVisiteAt(): ?\DateTimeInterface
    {
        return $this->last_visite_at;
    }

    public function setLastVisiteAt(\DateTimeInterface $last_visite_at): self
    {
        $this->last_visite_at = $last_visite_at;

        return $this;
    }

    /**
     * @return Collection|News[]
     */
    public function getNews(): Collection
    {
        return $this->news;
    }

    public function addNews(News $news): self
    {
        if (!$this->news->contains($news)) {
            $this->news[] = $news;
            $news->addVisitor($this);
        }

        return $this;
    }

    public function removeNews(News $news): self
    {
        if ($this->news->removeElement($news)) {
            $news->removeVisitor($this);
        }

        return $this;
    }

    /**
     * @return Collection|News[]
     */
    public function getNewsLiked(): Collection
    {
        return $this->news_liked;
    }

    public function addNewsLiked(News $newsLiked): self
    {
        if (!$this->news_liked->contains($newsLiked)) {
            $this->news_liked[] = $newsLiked;
            $newsLiked->addLike($this);
        }

        return $this;
    }

    public function removeNewsLiked(News $newsLiked): self
    {
        if ($this->news_liked->removeElement($newsLiked)) {
            $newsLiked->removeLike($this);
        }

        return $this;
    }

    /**
     * @return Collection|Message[]
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setVisitor($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->removeElement($message)) {
            // set the owning side to null (unless already changed)
            if ($message->getVisitor() === $this) {
                $message->setVisitor(null);
            }
        }

        return $this;
    }
}
