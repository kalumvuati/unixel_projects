//JS Script Materialize
import './materialize.min';

//JQ Script Dropzone
import 'dropzone';

//Import Text Editor from QuillJs.com
//import Quill from 'quill';
const $b    = (selector) => document.querySelector(selector);
const $b_   = (selector) => document.querySelectorAll(selector);

//PORTFOLIO -Réf

var arrows_left = "";
var arrows_right= "";
const carousel  = document.getElementsByClassName("my_carousel");
const container = document.getElementsByClassName("carousel_container");
const container_target    = document.querySelector("#container_target");

window.addEventListener("load", callback_portfolio_ref);

//Prise en charge de touches lors de navegation sur le portfolio_reference
window.addEventListener('keydown',keypress_check);

//ACtivation de Dropdown
document.addEventListener('DOMContentLoaded', function () {
    var elems_dropdown_trigger = $b_('.dropdown-trigger');
    var instances_ropdown_trigger = M.Dropdown.init(elems_dropdown_trigger, {});

    //activation de Side Bar
    var elems_sidenav = $b_('.sidenav');
    var instances_sidenav = M.Sidenav.init(elems_sidenav, {});

    //Navbar Buton
    var elems_fixed = document.querySelectorAll('.fixed-action-btn');
    var instances_elems_fixed = M.FloatingActionButton.init(elems_fixed, {
        direction: 'top',
        hoverEnabled: false
    });

    //NEWS - Open Comments
    var elems_collapsible = document.querySelectorAll('.collapsible');
    var instances_collapsible = M.Collapsible.init(elems_collapsible, {});

    /**SLIDER FAIT MAISON*/
    /**
     * @brief: permet d'enlever de remplacer une image en
     * gardant l'effet/animation de AOS
     */
    var elems_slider = document.querySelectorAll('.slider');
    var instances_slider = M.Slider.init(elems_slider, null);

    //NEWS Comments
    const comments = $b_(".news-comments>span>i.fa-comment");
    comments.forEach(comment => {
        comment.addEventListener("click", showCommentToggle);
    });

    //IMAGE
    //View image
    //Todo :  rensembler toutes les parties des Event qui commence par cette ligne ci-dessous
    var elems_materialboxed = document.querySelectorAll('.materialboxed');
    var instances_materialboxed = M.Materialbox.init(elems_materialboxed, {});

    // GALLERY------------------------Select field-------------------------
    var elems_select = document.querySelectorAll('select');
    var instances_select = M.FormSelect.init(elems_select, {});

    //For Tool Tips ---- Hover Message
    var tooltipped_tooltipped = document.querySelectorAll('.tooltipped');
    var instances_tooltipped = M.Tooltip.init(tooltipped_tooltipped, {});

});

/**
 * ANIMATION OF PAGES
 */
AOS.init({
    delay: 50,
    duration: 500
});

//Search Bat for news Page
search();

// Input
M.updateTextFields();

/*AUTOR LiKE heart  - ARTICLE PAGE*/
try {
    const auto_like_all = $b_('.autor-like');
    auto_like_all.forEach(auto_like => {
        auto_like.addEventListener("click", (e) => {
            e.preventDefault();
            const id = e.target.id;
            if (e.target.classList.contains('far')) {
                //like
                addLikeToggle(id);
                e.target.classList.remove('far')
                e.target.classList.add('fas');
                //update the number os like ++
            } else {
                //unlike
                addLikeToggle(id);
                e.target.classList.remove('fas')
                e.target.classList.add('far');
                //update the like's number --
            }
        })
    });

} catch (e) {
    console.log("This is not error");
}

//---------------------- EXCLUSIVE PAGE ARTICLE ---------------------------
/**
 * autor more vert
 * Traitement de cliques sur les 3 bouttons à droit du nom de l'auteur de l'article
 */
const autor_show_hide_info = $b_(".author-more-vert");
autor_show_hide_info.forEach((item) => {

    item.addEventListener("click", (e) => {
        e.preventDefault();
        const close_id = e.target.offsetParent.getElementsByClassName('card-content-b').item(0);
        if (close_id && close_id.firstElementChild && close_id.firstElementChild.firstElementChild) {
            close_id.firstElementChild.firstElementChild.addEventListener("click", (e) => {
                close_id.style.display = "none";
                first_element.style.display = "block";
            })
        }
        const first_element = e.target.offsetParent.firstElementChild
        first_element.style.display = "none";
        close_id.style.display = "block";
        //Close

    })

});

//Todo - à refaire car, ça ne marche plus à cause de changement de structure de class
//When a picture was clicked
const cards = $b_(".card-image img");
cards.forEach((card) => {
    card.addEventListener("click", (e) => {
        if (e.target.offsetParent && e.target.offsetParent.offsetParent &&
            e.target.offsetParent.offsetParent.offsetParent) {

            const old_content_status = $b(".portofolio-gallery").cloneNode(true);
            //Add a light-box class to the current element
            const old_target = e.target;
            removeSelector(".galery-light-box");
            e.target.offsetParent.offsetParent.offsetParent.classList.add("galery-light-box");

            //close
            $b(".galery-light-box").addEventListener("click", (e) => {
                if (old_target !== e.target) {
                    removeSelector(".galery-light-box");
                }
            })
        }
    })
});

//Todo - to  activate
// //Carousel
// //Drop and drag
Dropzone.autoDiscover = false;
$(document).ready(function () {

    //Todo - enveler la répétition de DOMContentLoaded

    //Todo utilisé alors qu'il n'est pas en place
    // const $element = $('.js-reference-list');
    const $element = $('.collection');
    if ($element && $element.length> 0) {
        var referenceList = new ReferenceList($element);
        initializeDropzone(referenceList);
    }
});

moveCheckBox();

// GENERAL ------------------- SCROLL
window.onscroll = () => {
    scrollUp();
}

$(".go_up").on('click', e => {
    e.preventDefault();
    $('html, body').animate({ scrollTop: 0 }, '300');
})

// ALERT CLOSE --------------------------------------
const alert_close = $b(".alert-close");
if (alert_close) {
    alert_close.addEventListener('click', e => {
        e.preventDefault();
        if (e.target.parentElement) {
            e.target.parentElement.remove();
        }
    })
}

// NEWS--------------------------------
function addLikeToggle(id) {
    const url = `/news/addlike/${id}`;
    const newsnbLike = `#newsNbLike-${id}`;
    $.ajax({
        url: url,
        method: 'GET'
    }).then((data) => {
        $b(newsnbLike).textContent = parseInt(data);
    });
}

function showCommentToggle(e) {
    e.preventDefault();
    const tabSplited = this.id.split('-');

    const currentCommentId = tabSplited[tabSplited.length - 1];
    const cardId = `#news-all-comments-${currentCommentId}`;
    const element = $b(cardId);
    if (element && element.classList.contains("hide")) {
        element.classList.remove('hide');
        element.classList.add('show');

    } else {
        element.classList.remove('show');
        element.classList.add('hide');
    }

}

function showcase_slider() {
    const element_class = $b('.showcase');
    // const bgs = `${JSON.parse(element_class.dataset.slideImage)}`;

    const paysage_images_label_size = 10;
    let compteur = 0;

    if (element_class) {

        setInterval(function () {

            remove(element_class)
            setTimeout(() => {
                add(element_class);
                compteur = compteur % paysage_images_label_size + 1;
                element_class.style.background = `url("images/paysage/paysage-${compteur}.jpg") no-repeat center center/cover`;
                compteur++;
                add(element_class)
            }, 500)

        }, 5000);
    }
}

function remove(element_class) {
    element_class.classList.remove('aos-init');
    element_class.classList.remove('aos-animate');
}

function add(element_class) {
    element_class.classList.add('aos-init');
    element_class.classList.add('aos-animate');
}

//Event on writting
function search() {
    const articles = $b_('.articles>div');
    if (!articles.length)
        return;
    setEvent('#search', true, 'keyup', (e) => {

        articles.forEach(element => {

            if (element.textContent.toLowerCase().indexOf(e.target.value.toLowerCase()) !== -1)
                element.style.display = "block";
            else
                element.style.display = "none";
        })
    });
}

function setEvent(selector, one = true, action, callback) {
    if (one) {
        $b(selector).addEventListener(action, (e) => callback(e));
        return;
    }
    $b_(selector).forEach(element => {
        element.addEventListener(action, (e) => callback(e));
    })
}

//----------------------- GALERY-------------------------------------------

/**
 * @param {ReferenceList} referenceList
 */
function initializeDropzone(referenceList) {

    var references = $b('.js-reference-dropzone');

    if (!references) {
        return;
    }
    var dropzone = new Dropzone(references, {
        paramName: 'reference',
        init: function () {
            this.on('error', function (file, data) {
                if (data.detail) {
                    this.emit("error", file, data.detail);
                }
            });
            this.on('success', function (file, data) {
                referenceList.addReference(data);
            });
        }
    });
}
// PORTFOLIO - REFERENCES
// todo - use Webpack Encore so ES6 syntax is transpiled to ES5
class ReferenceList {
    constructor($element) {
        this.$element   = $element;
        this.references = [];
        this.id         = this.$element.data('id');
        this.sortable   = Sortable.create(this.$element[0], {
            handle: '.drag-handle',
            animation: 150,
            onEnd: () => {
                $.ajax({
                    url: '/admin/portfolio/references/reorder/'+this.id,
                    method: 'POST',
                    success: this.success,
                    data: JSON.stringify(this.sortable.toArray())
                });
            }
        });
        this.render();
        $.ajax({
            url: this.$element.data('url')
        }).then(data => {
            //Todo Error : une reponse d'une page => Une erreur
            // console.log(this.$element.data('url'),"Source d'erreur : ", data)
            this.references = data;
            this.render();
        });

        //deleting
        this.$element.on('click', '.js-reference-delete', (event) => {
            var id = "";
            if (event.target.id)
                id = event.target.id;
            else
                id = event.target.parentElement.id;

            this.handleReferenceDelete(event, id);
        });
    }

    success() {
        console.log("Envoi d'Ajax avec succès!");
    }

    handleReferenceDelete(event, id) {
        var id_num = id.split("-")[id.split("-").length - 1]
        const $li = document.querySelector(`#collection-item-${id_num}`);

        $.ajax({
            url: '/admin/reference/delete/' + id_num,
            method: 'DELETE'
        }).then(() => {
            this.references = this.references.filter(reference => {
                return parseInt(reference.id) !== parseInt(id_num);
            });
            this.render();
        });
    }

    addReference(reference) {

        if (this.references) {
            this.references.push(reference);
            this.render();
        }
    }

    render() {
        /*
        Cette ligne à rajouter dans le return lors qu'on souhaite permettre à l'admin de faire le download des images
           <a href="/admin/portfolio/references/${reference.id}/download" class="secondary-content" data-id="${reference.id}">
                <i class="fas fa-download"></i>
            </a>
         */
        const itemsHtml = this.references.map(reference => {
            return `
                <li class="collection-item" id="collection-item-${reference.id}" data-url="/admin/portfolio/${reference.id}">
                    <span class="drag-handle fas fa-arrows-alt" ></span>
                    <div>${reference.originalFilename}</div>
                    <button class="js-reference-delete btn red" id="reference-${reference.id}" >
                        <i class="fa fa-trash"></i>
                    </button>
                </li>`
        });
        this.$element.html(itemsHtml.join(''));
    }
}

function callback_portfolio_ref(envet) {
    //Evenement pour activation de carousel - gallery
    if (carousel && carousel[0]) {
        const images = carousel[0].getElementsByTagName('img');
        for (var i = 0; i < images.length; i++) {
            images[i].addEventListener('click', carouselToggle_portfolio_ref);
        }
    }

    //Evennement sur les fèches de defillement
    arrows_left   = document.querySelector('.fa-arrow-left');
    arrows_right  = document.querySelector('.fa-arrow-right');
    //Evennement
    if (arrows_left && arrows_right) {
        arrows_left.addEventListener('click', switch_left_portfolio_ref);
        arrows_right.addEventListener('click', switch_right_portfolio_ref);
    }
}

//Prise en charge lors qu'on clique sur les touches de directions [left, right]
function keypress_check(e) {
    const activated_element = document.querySelector('.active_ref');
    if (activated_element) {
        if ( e.key === "ArrowLeft") {
            switch_left_portfolio_ref(e);
        }else if ( e.key === "ArrowRight") {
            switch_right_portfolio_ref(e);
        }else if (e.key == "Escape") {
            activated_element.classList.remove('active_ref');
            container_target.classList.add('carousel_container');
            document.querySelector('.arrows').style.display     = "none";
            document.querySelector('.main-title').style.display = "block";
            document.querySelector('body').style.overflow       = "visible";
            //Remettre la taille de l'image en format portrait en 100%
            if (activated_element.getElementsByTagName('img')[0].classList.contains('portrait') ||
                activated_element.getElementsByTagName('img')[0].classList.contains('paysage')) {
                // activated_element.getElementsByTagName('img')[0].style.width = "100%";
                activated_element.getElementsByTagName('img')[0].id = "";
            }
        }
    }
}

function switch_left_portfolio_ref(e) {
    const current_image_active  = document.querySelector('.my_card.active_ref');
    const previous_element      = current_image_active.previousElementSibling;
    setActive_portfolio_ref(current_image_active,previous_element);
}

function switch_right_portfolio_ref(e) {
    // const images_list = document.querySelectorAll('.my_card');
    const current_image_active = document.querySelector('.my_card.active_ref');
    const next_element         = current_image_active.nextElementSibling;
    setActive_portfolio_ref(current_image_active,next_element);
}

function setActive_portfolio_ref(current_image_active,image_target_element) {

    //Remettre la taille de l'image en format portrait en  100%
    if (image_target_element && current_image_active.getElementsByTagName('img')[0].classList.contains('portrait') ||
        image_target_element && current_image_active.getElementsByTagName('img')[0].classList.contains('paysage')) {
        // current_image_active.getElementsByTagName('img')[0].style.width = "100%";
        current_image_active.getElementsByTagName('img')[0].id = "";
    }

    if (image_target_element) {
        const actv_element_div = document.querySelector('.active_ref');
        if (actv_element_div) {
            actv_element_div.classList.remove('active_ref');
        }
        if (image_target_element.getElementsByTagName('img')[0].classList.contains('portrait')) {
            // image_target_element.getElementsByTagName('img')[0].style.width = "30%";
            image_target_element.getElementsByTagName('img')[0].id = "portrait";
        }else {
            image_target_element.getElementsByTagName('img')[0].id = "paysage";
        }
        image_target_element.classList.add('active_ref');
    }
}

function carouselToggle_portfolio_ref(e) {
    e.preventDefault();

    const oldCarouselElement  = document.querySelector('.active_ref');

    //Enlever la classe active où elle se trouvait avant
    if (oldCarouselElement) {
        oldCarouselElement.classList.remove('active_ref');
    }

    //activation de la classe active à l'élément current
    e.currentTarget.parentElement.classList.add('active_ref');

    //Redimensionner la taille de l'image en paysage
    if (e.currentTarget.classList.contains('portrait')) {
        // e.currentTarget.style.width = "30%";
        e.currentTarget.id = "portrait";
    }else {
        e.currentTarget.id = "paysage";
    }

    //Animation de scrolling
    $("html, body").animate({ scrollTop: 0 }, 500);
    container_target.classList.remove('carousel_container');
    document.querySelector('body').style.overflow       = "hidden";
    document.querySelector('.arrows').style.display     = "flex";
    document.querySelector('.main-title').style.display = "none";

    //container.style.width="100vw";
    //Evennement pour la desctivation de carousel
    document.querySelector('.active_ref').addEventListener("click", e => {
        if (e.target && e.target.classList.contains('active_ref')) {
            e.currentTarget.classList.remove('active_ref');
            container_target.classList.add('carousel_container');
            document.querySelector('.arrows').style.display = "none";
            document.querySelector('body').style.overflow   = "visible";
            document.querySelector('.main-title').style.display = "block";
            //Remettre la taille de l'image en format portrait en 100%
            if (e.target.getElementsByTagName('img')[0].classList.contains('portrait') ||
                e.target.getElementsByTagName('img')[0].classList.contains('paysage')) {
                // e.target.getElementsByTagName('img')[0].style.width = "100%";
                e.target.getElementsByTagName('img')[0].id = "";
            }
        }
    });
}

//------------------------------------------------------------------
function removeSelector(selector, all = true) {
    const selector_name = selector.substring(1, selector.length);
    if (all) {
        $b_(selector).forEach((elem, index) => {

            if (elem.classList.contains(selector_name)) {
                elem.classList.remove(selector_name);
            }
        })
    } else {
        if ($b(selector)) {
            $b(selector).classList.remove(selector_name);
        }
    }
}
// PRICE -----------------------------------------------------------
/* Traitement du checkbox de price
//Cette fonction permet faire en sorte que l'affichage de Checkbox soit possible avec le Framework Materialize
*/
function moveCheckBox() {
    const parentDivInput = document.querySelector("#price-checkbox>div>input");
    const element = document.querySelector("#price-label");
    const span = document.createElement('span');
    span.textContent = "";
    if (element) {
        element.appendChild(parentDivInput);
        element.appendChild(span);
    }
}

/**
 * Elle affiche le boutton scroll Up lors
 * qu'on scrolle de 30%
 */
function scrollUp() {
    if ( $b('.go_up')) {
        if (document.body.scrollTop > 30 || document.documentElement.scrollTop > 20) {
            $b('.go_up').style.display = "block";
        } else {
            $b('.go_up').style.display = "none";
        }
    }
}
