<?php

namespace App\Tests\Unit;

use App\Entity\Admin;
use PHPUnit\Framework\TestCase;

class AdminTest extends TestCase
{
    /**
     * @dataProvider getProvidedValid
     */
    public function testValid(Admin $admin): void
    {
        $date = new \DateTime("now");
        $this->assertEquals('name', $admin->getName());
        $this->assertEquals('lastname', $admin->getLastname());
        $this->assertEquals('image.jpg', $admin->getImagefile());
        $this->assertEquals('jobtitle', $admin->getJobtitle());
        $this->assertEquals($admin->getBirthday()->format("d"), $date->format("d"));
        $this->assertEquals('email@email.com', $admin->getEmail());
        $this->assertEquals('password', $admin->getPassword());
    }

    /**
     * @dataProvider getProvidedValid
     */
    public function testNotValid(Admin $admin): void
    {
        $date = new \DateTime("+1 day");
        $this->assertNotEquals('false', $admin->getName());
        $this->assertNotEquals('false', $admin->getLastname());
        $this->assertNotEquals('imfalse', $admin->getImagefile());
        $this->assertNotEquals('false', $admin->getJobtitle());
        $this->assertNotEquals($admin->getBirthday()->format("d"), $date->format("d"));
        $this->assertNotEquals('false', $admin->getEmail());
        $this->assertNotEquals('false', $admin->getPassword());
    }

    /**
     * @dataProvider getProvidedEmpty
     */
    public function testEmpty(Admin $admin): void
    {
        $this->assertEmpty($admin->getName());
        $this->assertEmpty($admin->getLastname());
        $this->assertEmpty($admin->getImagefile());
        $this->assertEmpty($admin->getJobtitle());
        $this->assertEmpty($admin->getBirthday());
        $this->assertEmpty($admin->getEmail());
        $this->assertEmpty($admin->getPassword());
    }

    public function getProvidedValid(): array
    {
        $admins =  [];
        $nb_example = 5;
        for ($i = 0; $i < $nb_example ; $i++) {
            $admin = new Admin();
            $date = new \DateTime("now");
            $admin->setName("name")
                ->setLastname("lastname")
                ->setImagefile("image.jpg")
                ->setJobtitle("jobtitle")
                ->setBirthday($date)
                ->setEmail("email@email.com")
                ->setPassword("password");
            $admins [] = [$admin];
        }
        return $admins;
    }

    public function getProvidedEmpty(): array
    {
        $admins =  [];
        $nb_example = 5;
        for ($i = 0; $i < $nb_example ; $i++) {
            $admin = new Admin();
            $admin->setName("")
                ->setLastname("")
                ->setImagefile("")
                ->setJobtitle("")
                ->setEmail("")
                ->setPassword("");
            $admins [] = [$admin];
        }
        return $admins;
    }
}
