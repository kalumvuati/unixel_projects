<?php

namespace App\Tests\Unit;

use App\Entity\Gallery;
use PHPUnit\Framework\TestCase;

class GalleryTest extends TestCase
{
    /**
     * @dataProvider getProvidedValid
     */
    public function testValid(Gallery $gallery): void
    {
        $date = new \DateTime("now");
        $this->assertEquals('imagefile', $gallery->getImagefile());
        $this->assertEquals('mime', $gallery->getMimeType());
        $this->assertEquals('originalname', $gallery->getOriginalName());
        $this->assertEquals('imageformat', $gallery->getImageFormat());
        $this->assertEquals($gallery->getCreatedAt()->format("d"), $date->format("d"));
        $this->assertEquals(1, $gallery->getPosition());
    }

    /**
     * @dataProvider getProvidedValid
     */
    public function testNotValid(Gallery $gallery): void
    {
        $date = new \DateTime("+1 day");
        $this->assertNotEquals('false', $gallery->getImagefile());
        $this->assertNotEquals('false', $gallery->getMimeType());
        $this->assertNotEquals('false', $gallery->getImageFormat());
        $this->assertNotEquals('false', $gallery->getOriginalName());
        $this->assertNotEquals($gallery->getCreatedAt()->format("d"), $date->format("d"));
        $this->assertNotEquals(2, $gallery->getPosition());
    }

    /**
     * @dataProvider getProvidedEmpty
     */
    public function testEmpty(Gallery $gallery): void
    {
        $this->assertEmpty($gallery->getImagefile());
        $this->assertEmpty($gallery->getMimeType());
        $this->assertEmpty($gallery->getImageFormat());
        $this->assertEmpty($gallery->getOriginalName());
        $this->assertEmpty($gallery->getPosition());
    }

    public function getProvidedValid(): array
    {
        $galleries =  [];
        $nb_example = 5;
        for ($i = 0; $i < $nb_example ; $i++) {
            $gallery = new Gallery();
            $gallery->setImagefile("imagefile")
                ->setOriginalName("originalname")
                ->setMimeType("mime")
                ->setImageFormat("imageformat")
                ->setPosition(1);

            $galleries [] = [$gallery];
        }
        return $galleries;
    }

    public function getProvidedEmpty(): array
    {
        $galleries =  [];
        $nb_example = 5;
        for ($i = 0; $i < $nb_example ; $i++) {
            $gallery = new Gallery();
            $gallery->setImagefile("")
                ->setOriginalName("")
                ->setMimeType("")
                ->setImageFormat("");

            $galleries [] = [$gallery];
        }
        return $galleries;
    }

}
