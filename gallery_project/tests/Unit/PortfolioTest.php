<?php

namespace App\Tests\Unit;

use App\Entity\Portfolio;
use PHPUnit\Framework\TestCase;

class PortfolioTest extends TestCase
{
    /**
     * @dataProvider getProvidedValid
     */
    public function testValid(Portfolio $portfolio): void
    {
        $date = new \DateTime("now");
        $this->assertEquals('title', $portfolio->getTitle());
        $this->assertEquals(2, $portfolio->getNbLike());
        $this->assertEquals(2, $portfolio->getNbUnliked());
        $this->assertEquals('imagefile', $portfolio->getImagefile());
        $this->assertEquals($portfolio->getCreatedAt()->format("d"), $date->format("d"));
    }

    /**
     * @dataProvider getProvidedValid
     */
    public function testNotValid(Portfolio $portfolio): void
    {
        $date = new \DateTime("+1 day");
        $this->assertNotEquals('false', $portfolio->getTitle());
        $this->assertNotEquals(1, $portfolio->getNbLike());
        $this->assertNotEquals(1, $portfolio->getNbUnliked());
        $this->assertNotEquals('false', $portfolio->getImagefile());
        $this->assertNotEquals($portfolio->getCreatedAt()->format("d"), $date->format("d"));
    }

    /**
     * @dataProvider getProvidedEmpty
     */
    public function testEmpty(Portfolio $portfolio): void
    {
        $this->assertEmpty($portfolio->getTitle());
        $this->assertEmpty($portfolio->getNbLike());
        $this->assertEmpty($portfolio->getNbUnliked());
        $this->assertEmpty($portfolio->getImagefile());
    }

    public function getProvidedValid(): array
    {
        $portfolios =  [];
        $nb_example = 5;
        for ($i = 0; $i < $nb_example ; $i++) {
            $portfolio = new Portfolio();
            $portfolio->setImagefile("imagefile")
                ->setTitle('title')
                ->setNbLike(2)
                ->setNbUnliked(2)
                ->setImagefile('imagefile');
            $portfolios [] = [$portfolio];
        }
        return $portfolios;
    }

    public function getProvidedEmpty(): array
    {
        $portfolios =  [];
        $nb_example = 5;
        for ($i = 0; $i < $nb_example ; $i++) {
            $portfolio = new Portfolio();
            $portfolio->setImagefile("")
                ->setTitle('')
                ->setImagefile('');
            $portfolios [] = [$portfolio];
        }
        return $portfolios;
    }

}
