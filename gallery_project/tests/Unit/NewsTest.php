<?php

namespace App\Tests\Unit;

use App\Entity\News;
use DateTime;
use PHPUnit\Framework\TestCase;

class NewsTest extends TestCase
{
    /**
     * @dataProvider getProvidedValid
     */
    public function testValid(News $news): void
    {
        $date = new DateTime("now");
        $this->assertEquals('title', $news->getTitle());
        $this->assertEquals('description', $news->getDescription());
        $this->assertEquals('imagefile', $news->getImagefile());
        $this->assertEquals('videourl', $news->getVideoUrl());
        $this->assertEquals($date->format("m"), $news->getCreatedAt()->format("m"));
    }

    /**
     * @dataProvider getProvidedValid
     */
    public function testNotValid(News $news): void
    {
        $date = new DateTime("+1 day");
        $this->assertNotEquals('false', $news->getTitle());
        $this->assertNotEquals('false', $news->getDescription());
        $this->assertNotEquals('false', $news->getImagefile());
        $this->assertNotEquals('false', $news->getVideoUrl());
        $this->assertNotEquals($date->format("d"), $news->getCreatedAt()->format("d"));
    }

    /**
     * @dataProvider getProvidedEmpty
     */
    public function testEmpty(News $news): void
    {
        $this->assertEmpty($news->getTitle());
        $this->assertEmpty($news->getDescription());
        $this->assertEmpty($news->getImagefile());
        $this->assertEmpty($news->getVideoUrl());
    }

    public function getProvidedValid(): array
    {
        $news =  [];
        $nb_example = 5;
        for ($i = 0; $i < $nb_example ; $i++) {
            $new = new News();
            $new->setTitle('title')
                ->setDescription('description')
                ->setImagefile('imagefile')
                ->setVideoUrl('videourl');
            $news [] = [$new];
        }
        return $news;
    }

    public function getProvidedEmpty(): array
    {
        $news =  [];
        $nb_example = 5;
        for ($i = 0; $i < $nb_example ; $i++) {
            $new = new News();
            $new->setTitle('')
                ->setDescription('')
                ->setImagefile('')
                ->setVideoUrl('');
            $news [] = [$new];
        }
        return $news;
    }

}
