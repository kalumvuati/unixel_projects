<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        return $this->render('home/index.html.twig');
    }

    /**
     * @Route("/services", name="home_services")
     */
    public function services(): Response
    {
        return $this->render('home/services.html.twig');
    }

    /**
     * @Route("/feature", name="home_feature")
     */
    public function feature(): Response
    {
        return $this->render('home/feature.html.twig');
    }

    /**
     * @Route("/login", name="home_login")
     */
    public function login(): Response
    {
        return $this->render('home/login.html.twig');
    }
}
