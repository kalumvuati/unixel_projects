/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
//STYLES
import './styles/app.css';
import './styles/style.css';
import './styles/responsive.css';
import './styles/color-one.css';



//JAVASCRIPTS
import * as JQuery from 'jquery/dist/jquery.min';
import 'popper.js/dist/popper.min';
import 'bootstrap/dist/js/bootstrap.min';

import 'jquery.easing/jquery.easing.min';
import 'language-switcher/dist/index';
import 'jquery.appear/jquery.appear';
import 'jquery-countto/jquery.countTo';
import '@fancyapps/fancybox/dist/jquery.fancybox.min;
import 'owl.carousel/dist/owl.carousel.min';
import 'aos/dist/aos';
import './js/theme';

// start the Stimulus application
import './bootstrap';
