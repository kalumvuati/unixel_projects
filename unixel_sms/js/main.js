const float_js = document.querySelector("#float_js");

if (float_js) {
    float_js.addEventListener('mouseover', e=>{
        float_js.classList.add('active');
        animate_float('.float-inner-items li');
    })
    float_js.addEventListener('mouseleave', e=>{
        remove_animate_float('.float-inner-items li');
        window.setTimeout(e=>{
            float_js.classList.remove('active');
        }, 600);
    })
}
//Animation slider activation
//Instance Glide object
var glide = new Glide('.glide', {
    type: 'carousel',
    perView: 4,
    focusAt: 'center',
    autoplay: 2000,
    breakpoints: {
      800: {
        perView: 2
      },
      480: {
        perView: 1
      }
    }
  });
//Mount the animation
glide.mount()

//Initilisation of AOS
AOS.init({
  disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
  startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
  initClassName: 'aos-init', // class applied after initialization
  animatedClassName: 'aos-animate', // class applied on animation
  offset: 120, // offset (in px) from the original trigger point
  delay: 50, // values from 0 to 3000, with step 50ms
  duration: 400, // values from 0 to 3000, with step 50ms
  easing: 'ease-in-out', // default easing for AOS animations
});

//Activate Humburger event on click
active_humburger();
humburger_close();

function animate_float(selectors)
{
    let list_items = document.querySelectorAll(selectors);
    
    //Reverse Array
    let newArray = new Array();
    list_items.forEach((item) => {
        newArray.unshift(item);
    });

    newArray.forEach((item, index)=>{
        let item_active = item;
        if (index == 0 && !item.classList.contains('active')) {
            item_active.classList.add('active');
        }else if (!item.classList.contains('active')) {
            window.setTimeout(item=>{
                item_active.classList.add('active');
            }, index*50);
        }
    });
}

function remove_animate_float(selectors)
{
    let list_items = document.querySelectorAll(selectors);

    list_items.forEach((item, index)=>{
        let item_active = item;
        if (index == 0 ) {
            item_active.classList.remove('active');
        }else {
            window.setTimeout(item=>{
                item_active.classList.remove('active');
            }, index*50);
        }
    });
}

function active_humburger()
{
    let html_element = document.querySelector('#humburger_js');
    if (html_element) {
        html_element.addEventListener("click", e=>{
            let humburger_nav = document.querySelector('#humburger_nav_js');
            if (humburger_nav) {
                humburger_nav.classList.add('active');
            }
        });
    }
}

function humburger_close()
{
    let element = document.querySelector('#humburger_nav_js .humburger-close');
    if (element) {
        element.addEventListener('click', e=>{
            let humburger_nav = document.querySelector('#humburger_nav_js');
            if (humburger_nav) {
                humburger_nav.classList.remove('active');
            } 
        })
    }
}